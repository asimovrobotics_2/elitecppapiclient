//     Copyright (c) 2012-2014 ASIMOV Robotics. All rights reserved. ////
//
// Filename:    main.cpp
//
//------------------------------------------------------------------------------
#include "include\dynamotion_custom_client.h"
int main(int argc,char *argv[])
{
	dynamotion_custom_client test;
	test.Connect("127.0.0.1",19999);
	float pos = 0.0;
	while(1)
	{
		//test.Set_Joint_Position("joint_1",-1.57);
		//test.Set_Joint_Position("joint_6",0.21);
		/*
		Setting joint position and joint velocity in edit box
		*/
		test.Set_Edit_Box();
		/*
		Reading joint_0 position
		*/
		pos = test.Get_Joint_Position("joint_1");
		std::cout<<"Joint:1 Position Reading:"<<pos<<"\n";;
	}

	return 0;
}