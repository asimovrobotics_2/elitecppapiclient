//     Copyright (c) 2012-2014 ASIMOV Robotics. All rights reserved. ////
//
// Filename:    dynamotion_custom_client.h
//
// Description: This class contain functions for interfacing
//              dynamotion custom client to dynamotion server
//
//------------------------------------------------------------------------------

#pragma once

#include <windows.h>
#include <math.h>
#include <stdio.h>
#include <conio.h>
#include <stdlib.h>
#include <iostream>


extern "C" {
    #include "extApi.h"
/*	#include "extApiCustom.h" if you wanna use custom remote API functions! */
}


class dynamotion_custom_client
{
public:
    ///Constructor
    dynamotion_custom_client(void);

    ///Connect function
    int Connect(char* ip,int port);

	///Connection Status
	int Connection_Status();

    ///Disconnect function
    void Disconnect(void);

    ///Get Joint Position function
    float  Get_Joint_Position(char* joint_name);

    ///Set Joint Position function
    int Set_Joint_Position(const char* joint_name,float position);

	///Set Edit Box in User Interface
	int Set_Edit_Box();

    ///Destructor
    ~dynamotion_custom_client(void);

        /// Handles and button variables
        int handle;
        int error_code_1;
        int arm_handle;
        int button_variable;
        int aux_variable;
        bool connection_status;
        int client_id ;
        float joint_velocity;
};
