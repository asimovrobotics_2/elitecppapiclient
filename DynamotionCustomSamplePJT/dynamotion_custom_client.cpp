//     Copyright (c) 2012-2014 ASIMOV Robotics. All rights reserved. ////
//
// Filename:    dynamotion_custom_client.cpp
//
//------------------------------------------------------------------------------


#include "include/dynamotion_custom_client.h"


/**
      * dynamotion_custom_client constructor 
*/

dynamotion_custom_client::dynamotion_custom_client(void)
{
        client_id = 0;   ///Fix , for getting client id 0 
        //button_variable = -1;
}

dynamotion_custom_client::~dynamotion_custom_client(void)
{
}

///////////////////////////////////////////////////////////////////////////////////////////////////////////

/**
       * Connect function connects custom_client client to dynamotion server.It takes two arguements 
       * @param ip Address in which server located.
       * @param port Port at which dynamotion server listening.
       * @return Status of connection
*/


int dynamotion_custom_client::Connect(char *ip,int port)
{
		//simxFinish(client_id);
		
		client_id = simxStart(ip,port,true,true,2000,5);
        
        printf("Client id %d \n",client_id);
        if (client_id != -1)
	    {
        
            printf("Connected to server %s \t Port = %d\n",ip,port);
            if (simxGetConnectionId(client_id)!=-1)
		        {
                printf("Got connnection id = %d\n",simxGetConnectionId(client_id));
                return 1;  
                }
            else{

                return -1;
                }
        }
		else
		{
			printf("Trying to Connect to Server \n");
			return -1;

		}



}

///////////////////////////////////////////////////////////////////////////////////////////////////////////
/**
       * Disconnect function disconnects dynamotion client from dynamotion_server.It takes no arguements and not returning any values  
*/
void dynamotion_custom_client::Disconnect(void)
{
    simxFinish(client_id);
    printf("Disconnect from Dynamotion Server\n");
   
}


///////////////////////////////////////////////////////////////////////////////////////////////////////////
/**
       * Get_Joint_Position function returns a joint value of a joint from dynamotion.It takes one arguement 
       * @param joint_name Name of the joint in which poistion need to get.
       * @return joint_value Joint position of the desired joint
*/

float dynamotion_custom_client::Get_Joint_Position(char* joint_name)
{

    ///Joint value variable
    float joint_value;
        
    try
   {
		int err_code = simxGetObjectHandle(client_id,joint_name,&handle,simx_opmode_streaming);
		simxGetJointPosition(client_id,handle,&joint_value,simx_opmode_streaming);

		if (simxGetJointPosition(client_id,handle,&joint_value,simx_opmode_buffer)==simx_error_noerror) 
		{ 
			// here we have the newest joint position in variable jointPosition!    
		}
		return (joint_value*57.295779513082320876798154814105);   //return in degrees
    }  
    catch(int e)
    {
        printf("Exception occured = %d\n",e);
    }
    
 return -1;

}

/*
///////////////////////////////////////////////////////////////////////////////////////////////////////////
/**
       * Set_Joint function set a joint value of a desired joint in dynamotion.It takes two arguement 
       * @param joint_name Name of the joint 
       * @param position Desired joint position.
       * @return result Success or Failure
*/


int dynamotion_custom_client::Set_Joint_Position(const char* joint_name,float position)
{
   
    int joint_handle;  
    try
    {
		int err_code = simxGetObjectHandle(client_id,joint_name,&joint_handle,simx_opmode_oneshot); // changed to oneshot
		return simxSetJointPosition(client_id,joint_handle,position,simx_opmode_oneshot);     //changed to one shot
    }
    catch(int e)
    {
        printf("Exception occured %d \n",e);
        return -1;
    }
    return -1;
}
int dynamotion_custom_client::Set_Edit_Box()
{
	
	int UI_Handle;
	int prop;
	try
	{
		simxGetUIHandle(client_id, "ManipulatorUI",&UI_Handle,simx_opmode_oneshot_wait);
		/*
		Set joint positions
		*/
		simxSetUIButtonLabel(client_id,UI_Handle,2,"-90.0","-90.0",simx_opmode_oneshot);
		simxSetIntegerSignal(client_id,"editbox_input",2,simx_opmode_oneshot_wait);

		simxSetUIButtonLabel(client_id,UI_Handle,1,"50.0","50.0",simx_opmode_oneshot);
		simxSetIntegerSignal(client_id,"editbox_input",1,simx_opmode_oneshot_wait);
		/*
		Set Joint velocity
		*/
		simxSetUIButtonLabel(client_id,UI_Handle,31,"8.0","8.0",simx_opmode_oneshot);
		simxSetIntegerSignal(client_id,"editbox_input",31,simx_opmode_oneshot_wait);
    }  
    catch(int e)
    {
        printf("Exception occured = %d\n",e);
    }    
	return -1;
}
